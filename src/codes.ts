import axios from "axios";
import {userMention} from '@discordjs/builders';
import {Message, TextChannel, ThreadChannel} from "discord.js";

import {allInvites} from "./invites";

export const parseCode = (code: string): string | undefined => {
  if (code.length !== 10) return undefined;
  return code;
};
export const handleCode = async (message: Message, code: string): Promise<void> => {
  let thread: ThreadChannel | undefined;
  if (message.channel instanceof TextChannel) {
    thread = await message.channel.threads.create({
      name: `Code redemption: ${code}`,
      autoArchiveDuration: 60,
      reason: 'Automatic code redemption status',
    });
  }

  const invites: {userId: string, inviteCode: string | null}[] = await allInvites();
  await Promise.all(invites.map(async ({inviteCode, userId}) => {
    console.log(`redeeming for ${userId} - ${inviteCode}`);
    const response = await axios.post('https://pcmob.parse.gemsofwar.com/parse/functions/submit_code_web', {
      NameCode: inviteCode,
      Code: code
    });
    const user = userMention(userId);
    const {result} = response.data;
    if (thread) {
      if (result.error) {
        await thread.send(`${user} failed to redeem the code`)
      } else {
        await thread.send(`${user} correctly redeemed the code`)
      }
    }
  }));
  await message.react('✅');
};
