import * as env from 'env-var';

import dotenv from 'dotenv';
dotenv.config()

const config = {
	channelId: env.get('CHANNEL_ID').required().asString(),
  clientId: env.get('CLIENT_ID').required().asString(),
  guildId: env.get('GUILD_ID').required().asString(),
  token: env.get('APP_TOKEN').required().asString(),
  redisUrl: env.get('REDIS_URL').required().asUrlString()
}

export default config;
