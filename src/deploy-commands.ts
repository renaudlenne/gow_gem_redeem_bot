import { SlashCommandBuilder, SlashCommandStringOption } from '@discordjs/builders';
import { REST } from '@discordjs/rest';
import { Routes } from 'discord-api-types/v9';
import config from './config';

const commands = [
	new SlashCommandBuilder().setName('register_code_redeemer')
		.setDescription('Register yourself to the code redeemer')
		.addStringOption((option: SlashCommandStringOption) =>
			option.setName('invite_code')
				.setDescription('Your GoW invite code')
				.setRequired(true))
]
	.map(command => command.toJSON());

const rest = new REST({ version: '9' }).setToken(config.token);

(async () => {
	try {
		await rest.put(
			Routes.applicationGuildCommands(config.clientId, config.guildId),
			{ body: commands },
		);

		console.log('Successfully registered application commands.');
	} catch (error) {
		console.error(error);
	}
})();
