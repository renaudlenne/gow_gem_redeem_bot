import * as redis from "redis";
import {User} from "discord.js";
import config from './config';

const redisClient = redis.createClient({
  socket: {
    url: config.redisUrl
  }
});

redisClient.on('error', (err) => console.log('Redis Client Error', err));


export const init = async (): Promise<void> => {
  await redisClient.connect()
};

export const registerInvite = async (user: User, invite: string): Promise<void> => {
  await redisClient.set(`uid:${user.id}`, invite)
};

export const allInvites = async (): Promise<{userId: string, inviteCode: string | null}[]> => {
  const allKeys = await redisClient.keys('uid:*');
  const allCodes = await redisClient.mGet(allKeys);
  return allKeys.map((key, idx) => ({userId: key.replace('uid:', ''), inviteCode: allCodes[idx]}))
};
