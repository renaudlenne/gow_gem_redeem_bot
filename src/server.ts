import {init} from "./invites";
import {handleCode} from "./codes";
import {parseCode} from "./codes";
import {registerInvite} from "./invites.js";

import {Client, Intents} from 'discord.js';
import config from './config.js';

// Create a new client instance
const client = new Client({intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES]});

// When the client is ready, run this code (only once)
client.once('ready', async () => {
  await init();
  console.log(`I am ready! Logged in as ${client.user?.tag}!`);
});

client.on('messageCreate', async message => {
  if (message.channel.id === config.channelId) {
    const code = parseCode(message.content)
    if (code) {
      await handleCode(message, code)
    }
  }
});


client.on('interactionCreate', async interaction => {
  console.log(`${interaction.user.tag} in #${interaction.channel} triggered an interaction.`);
  if (!interaction.isCommand()) return;

  const {user, commandName} = interaction;

  if (commandName === 'register_code_redeemer') {
    const inviteCode = interaction.options.getString('invite_code', true);
    await registerInvite(user, inviteCode);
    await interaction.reply(`Code successfully registered!`);
  }
});

// Login to Discord with your client's token
client.login(config.token).then(response =>
  console.log(`Logged-in: ${response}`)
);
